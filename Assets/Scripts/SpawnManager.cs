using System.Collections;
using UnityEngine;

namespace AlienShoot
{
    public class SpawnManager : MonoBehaviour
    {
        [SerializeField] private GameObject[] enemyPrefabs;
        [SerializeField] private float spawnInterval = 5f;
        [SerializeField] private float y;
        [SerializeField] private float spawnRangeX = 62;
        [SerializeField] private float spawnRangeXMines = 1;
        [SerializeField] private float spawnRangeZ = 26;
        [SerializeField] private float spawnRangeZMines = -17;
        private int enemyCount = 0;

        private void Start()
        {
            StartCoroutine(SpawnRandomEnemy());
        }


        private IEnumerator SpawnRandomEnemy()
        {
            while (true)
            {
                if (enemyCount > 4) yield break;
                enemyCount++;
                Vector3 spawnPosition = new Vector3(Random.Range(spawnRangeXMines, spawnRangeX), y,
                    Random.Range(spawnRangeZMines, spawnRangeZ));
                int enemyIndex = Random.Range(0, enemyPrefabs.Length);
                Instantiate(enemyPrefabs[enemyIndex], spawnPosition, enemyPrefabs[enemyIndex].transform.rotation);
                yield return new WaitForSeconds(spawnInterval);
            }
        }
    }

}