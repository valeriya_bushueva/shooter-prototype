using UnityEngine;

namespace AlienShoot
{
    public class DetectCollisionsEnemy : MonoBehaviour
    {
        private GameManager gameManager;
        private int pointValue = 1;

        private void Start()
        {
            gameManager = GameObject.Find(ConstantData.GameManagerName).GetComponent<GameManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            gameManager.RefreshScore(pointValue);
            SoundManager.Instance.PlayEnemyDeath();
            if (other.CompareTag(ConstantData.Tags.Player))
            {
                other.GetComponent<PlayerController>().TakeDamage(ConstantData.ToPlayerDamage);
            }
            else if (other.CompareTag(ConstantData.Tags.Bullet))
            {
                Destroy(gameObject);
                Destroy(other.gameObject);
            }

        }
    }
}
