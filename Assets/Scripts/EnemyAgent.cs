using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace AlienShoot
{
    public class EnemyAgent : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent agent;
        [SerializeField] private Transform seeRadiusPoint;
        [SerializeField] private Transform[] enemyTarget = new Transform[3];
        private int index;
        private bool seePlayer;
        private PlayerController player;

        private float PlayerDistance
        {
            get { return Vector3.Distance(player.transform.position, transform.position); }
        }

        private float SeeRadius
        {
            get { return Vector3.Distance(seeRadiusPoint.position, transform.position); }
        }

        private void Start()
        {
            agent.SetDestination(enemyTarget[index].position);
            player = FindObjectOfType<PlayerController>();
            StartCoroutine(EnemyAI());
        }

        private IEnumerator EnemyAI()
        {
            while (true)
            {
                yield return null;

                if (PlayerDistance < SeeRadius)
                {
                    agent.SetDestination(player.transform.position);
                    seePlayer = true;
                    yield return new WaitForSeconds(1);
                }
                else
                {
                    if (seePlayer)
                    {
                        agent.SetDestination(transform.position);
                        yield return new WaitForSeconds(2);

                        seePlayer = false;
                        agent.SetDestination(enemyTarget[index].position);
                    }

                    if (Vector3.Distance(transform.position, enemyTarget[index].position) < 3)
                    {
                        index++;
                        if (index >= enemyTarget.Length)
                        {
                            index = 0;
                        }

                        agent.SetDestination(enemyTarget[index].position);
                    }

                }
            }
        }
    }

}