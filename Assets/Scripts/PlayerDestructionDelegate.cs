using UnityEngine;

namespace AlienShoot
{
    public class PlayerDestructionDelegate : MonoBehaviour
    {
        public delegate void PlayerDelegate(GameObject player);

        public PlayerDelegate playerDelegate;

        private void OnDestroy()
        {
            if (playerDelegate != null)
            {
                playerDelegate(gameObject);
            }
        }
    }

}