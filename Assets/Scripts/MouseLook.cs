using UnityEngine;

namespace AlienShoot
{
    public class MouseLook : MonoBehaviour
    {
        [SerializeField] private float mouseSensetivity = 100f;
        [SerializeField] private Transform playerBody;
        [SerializeField] private float xMaxRotation = 0;
        private float xRotation = 0;

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        private void Update()
        {
            float mouseX = Input.GetAxis("Mouse X") * mouseSensetivity * Time.deltaTime;
            float mouseY = Input.GetAxis("Mouse Y") * mouseSensetivity * Time.deltaTime;

            xRotation -= mouseY;
            xRotation = Mathf.Clamp(xRotation, -90f, xMaxRotation);

            transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
            playerBody.Rotate(Vector3.up * mouseX);
        }
    }

}