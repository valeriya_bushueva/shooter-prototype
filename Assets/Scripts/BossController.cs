using UnityEngine;

namespace AlienShoot
{
    public class BossController : MonoBehaviour
    {
        [SerializeField] private int maxHealth = 100;
        [SerializeField] private int currentHealth;
        [SerializeField] private HealthBar healthBar;

        private void Start()
        {
            currentHealth = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
        }

        public void TakeDamage(int damage)
        {
            currentHealth -= damage;
            healthBar.SetHealth(currentHealth);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(ConstantData.Tags.Bullet))
            {
                TakeDamage(ConstantData.ToBossDamage);
            }

            if (currentHealth <= 0)
            {
                Destroy(gameObject);
            }

            if (other.CompareTag(ConstantData.Tags.Player))
            {
                other.GetComponent<PlayerController>().TakeDamage(1);
            }
        }
    }

}