﻿using System.Collections;
using UnityEngine;

namespace AlienShoot
{
    public class Grenade : MonoBehaviour
    {
        [SerializeField] private Rigidbody rigidbody;
        [SerializeField] private float throwSpeed;
        [SerializeField] private GameObject explodeEffectPrefab;
        [SerializeField] private float timeToExplode;
        [SerializeField] private float explosionScale;

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(timeToExplode);
            var explosion = Instantiate(explodeEffectPrefab, transform.position + Vector3.up * 1, explodeEffectPrefab.transform.rotation);
            explosion.transform.localScale = Vector3.one * explosionScale;
            Destroy(gameObject);
        }

        /// <summary>
        /// Prefab's Factory method
        /// </summary>
        public void InstantiateAndThrowGrenade(Vector3 throwFromPosition, Vector3 direction)
        {
            var newGrenade = Instantiate(this, throwFromPosition, transform.rotation);

            newGrenade.rigidbody.velocity = direction * throwSpeed;
        }
    }
}