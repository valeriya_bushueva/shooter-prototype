﻿namespace AlienShoot
{
    public static class ConstantData
    {
        public const int ToBossDamage = 10;
        public const int ToPlayerDamage = 1;
        public const string GameManagerName = "GameManager";
        
        public static class Tags
        {
            public const string Key = "Key";
            public const string Heart = "Heart";
            public const string Bullet = "Bullet";
            public const string Player = "Player";
        }

        public static class Audio
        {
            public const float DefaultVolume = 1.0f;
        }
    }
}