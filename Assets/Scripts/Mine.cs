﻿using UnityEngine;

namespace AlienShoot
{
    public class Mine : MonoBehaviour
    {
        [SerializeField] private GameObject explosionEffectPrefab;
        [SerializeField] private float explodeForce;
        [SerializeField] private Transform explodeRadiusTarget;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(ConstantData.Tags.Player))
            {
                Explode();
            }
        }
        
        public void Explode()
        {
            var colliders = Physics.OverlapSphere(transform.position, Vector3.Distance(explodeRadiusTarget.position, transform.position));

            if (colliders.Length > 0)
            {
                for (var i = 0; i < colliders.Length; i++)
                {
                    if (colliders[i].TryGetComponent<Rigidbody>(out var other))
                    {
                        var explodeDirection = (other.position - transform.position).normalized;
                        other.velocity = explodeDirection * explodeForce;
                    }
                }
            }

            var explosion = Instantiate(explosionEffectPrefab, transform.position, explosionEffectPrefab.transform.rotation);
            explosion.transform.localScale = Vector3.one * 3;
            Destroy(gameObject);
        }
    }
}