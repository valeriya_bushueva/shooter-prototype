using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace AlienShoot
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private float speed = 10f;
        [SerializeField] private GameObject projectilePrefab;
        [SerializeField] private float jumpForce;
        [SerializeField] private float gravityModifier;
        [SerializeField] private float jumpTimer;
        [SerializeField] private GameObject key;
        [SerializeField] private GameObject pickUpHeart;
        [SerializeField] private GameObject endDoor;
        [SerializeField] private GameObject endDoorLight;
        [SerializeField] private GameObject key0;
        [SerializeField] private GameObject heart0;
        [SerializeField] private Grenade grenadePrefab;
        [SerializeField] private Transform throwPositionFrom;
        [SerializeField] private GameObject minePrefab;
        [SerializeField] private TextMeshProUGUI gameOverText;
        [SerializeField] private AudioClip pickUpKeySound;
        [SerializeField] private AudioClip pickUpHeartSound;
        [SerializeField] private int maxHealth = 5;
        [SerializeField] private int currentHealth;
        [SerializeField] private HealthBar healthBar;
        [SerializeField] private string nextLevel;
        
        private float horizontalInput;
        private float verticalInput;
        private bool isOnGround = true;
        private bool jumping;
        private Rigidbody playerRb;
        private AudioSource playerAudio;

        private void Start()
        {
            playerRb = GetComponent<Rigidbody>();
            Physics.gravity *= gravityModifier;
            playerAudio = GetComponent<AudioSource>();
            currentHealth = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
        }
        
        private void Update()
        {
            PlayerMovement();
            PutMineByThePlayer();
            ThrowGrenades();
            Shooting();
            Jump();
        }

        private void PlayerMovement()
        {
            horizontalInput = Input.GetAxis("Horizontal");
            verticalInput = Input.GetAxis("Vertical");
            playerRb.velocity = transform.rotation * new Vector3(horizontalInput * speed, playerRb.velocity.y, verticalInput * speed);
        }

        private void PutMineByThePlayer()
        {
            if (Input.GetKeyDown(KeyCode.M))
            {
                Instantiate(minePrefab, GetPointOnSurface(), minePrefab.transform.rotation);
            }
        }

        private void ThrowGrenades()
        {
            if (Input.GetMouseButtonDown(1))
            {
                grenadePrefab.InstantiateAndThrowGrenade(throwPositionFrom.position, throwPositionFrom.forward);
            }
        }

        private void Shooting()
        {
            if (Input.GetButtonDown("Fire1") && !PauseMenu.GameIsPaused)
            {
                Instantiate(projectilePrefab, throwPositionFrom.position, throwPositionFrom.rotation);
            }
        }

        private Vector3 GetPointOnSurface()
        {
            if (Physics.Raycast(new Ray(throwPositionFrom.position, throwPositionFrom.forward), out var hit))
            {
                return hit.point;
            }

            return throwPositionFrom.position;
        }
        
        private void Jump()
        {
            if (Input.GetKeyDown(KeyCode.Space) && isOnGround)
            {
                isOnGround = false;
                jumping = true;
            }
            else if (Input.GetKeyUp(KeyCode.Space))
            {
                jumping = false;
            }

            if (jumping)
            {
                if (jumpTimer > 0)
                {
                    playerRb.velocity = new Vector3(playerRb.velocity.x, jumpForce, playerRb.velocity.z);
                    jumpTimer -= Time.deltaTime;
                }
            }
            else
            {
                jumpTimer = 0.5f;
            }
        }

        public void TakeDamage(int damage)
        {
            currentHealth -= damage;
            healthBar.SetHealth(currentHealth);

            TryGameOver();
        }

        public void TryGameOver()
        {
            if (currentHealth <= 0)
            {
                gameOverText.gameObject.SetActive(true);
            }
        }

        private void OnCollisionEnter(Collision other)
        {
            isOnGround = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(ConstantData.Tags.Key))
            {
                key0.SetActive(true);
                Destroy(other.gameObject);
                playerAudio.PlayOneShot(pickUpKeySound, ConstantData.Audio.DefaultVolume);
                endDoor.SetActive(false);
                endDoorLight.SetActive(true);
                
                SceneManager.LoadScene(nextLevel);
            }

            if (other.CompareTag(ConstantData.Tags.Heart))
            {
                heart0.SetActive(true);
                Destroy(other.gameObject);
                playerAudio.PlayOneShot(pickUpHeartSound, ConstantData.Audio.DefaultVolume);
            }
        }
    }
}
