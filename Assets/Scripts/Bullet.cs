﻿using System.Collections;
using UnityEngine;

namespace AlienShoot
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private float delayToDestroySeconds = 3;
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(delayToDestroySeconds);
            Destroy(gameObject);
        }
    }
}