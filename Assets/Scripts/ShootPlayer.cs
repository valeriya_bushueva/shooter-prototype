using System.Collections;
using UnityEngine;

namespace AlienShoot
{
    public class ShootPlayer : MonoBehaviour
    {
        [SerializeField] private GameObject player;
        [SerializeField] private Transform monster;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private float spawnInterval = 1f;
        [SerializeField] private GameObject bullet;
        private bool playerFound;

        private void OnPlayerDestroy(GameObject player)
        {
            Destroy(player);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(ConstantData.Tags.Player))
            {
                PlayerDestructionDelegate player = other.GetComponent<PlayerDestructionDelegate>();

                StartCoroutine(SpawnBullet());
                other.GetComponent<PlayerController>().TakeDamage(ConstantData.ToPlayerDamage);
                Debug.Log("Обнаружен игрок!");

            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(ConstantData.Tags.Player))
            {
                PlayerDestructionDelegate player = other.GetComponent<PlayerDestructionDelegate>();
                StopAllCoroutines();
                Debug.Log("Потерян игрок!");
            }
        }

        private void Update()
        {
            if (monster == null || playerTransform == null)
            {
                return;
            }

            Vector3 relativePosition = playerTransform.position - monster.position;
            Quaternion rotation = Quaternion.LookRotation(relativePosition);
            monster.rotation = rotation;
        }

        private IEnumerator SpawnBullet()
        {
            while (true)
            {
                Instantiate(bullet, monster.position, monster.transform.rotation);
                yield return new WaitForSeconds(spawnInterval);
            }
        }
    }

}