﻿using UnityEngine;

namespace AlienShoot
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private AudioClip enemyDeadClip;
        [SerializeField] private AudioSource source;
        public static SoundManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        
        public void PlayEnemyDeath()
        {
            source.PlayOneShot(enemyDeadClip, ConstantData.Audio.DefaultVolume);   
        }
    }
}