using UnityEngine;

namespace AlienShoot
{
    public class MoveForward : MonoBehaviour
    {
        [SerializeField] private float speed = 30f;

        private void Update()
        {
            transform.Translate(Vector3.forward * Time.deltaTime * speed);
        }
    }
}
