using System.Collections;
using UnityEngine;
using TMPro;

namespace AlienShoot
{
    public class GameManager : MonoBehaviour
    {
        private int score;
        [SerializeField] private TextMeshProUGUI scoreText;

        private void Start()
        {
            score = 0;
            RefreshScore(0);
        }

        public void RefreshScore(int scoretoAdd)
        {
            score += scoretoAdd;
            scoreText.text = "Score:" + score;
        }


    }


}
